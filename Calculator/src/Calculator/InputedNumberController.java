package Calculator;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.ArrayList;

public class InputedNumberController
		extends CalculatorMain {

	String numberOne = ""; // 초기값용 문자열
	String numberTwo = ""; // 그 다음 값들용 문자열
	String operation = ""; // 연산자용 문자열
	String recordLabel = ""; // 기록 라벨용 문자열
	String value = ""; // 마지막 계산을 여기에 저장할것
	List<String> number = new ArrayList<String>(); // 숫자용 리스트
	List<String> oper = new ArrayList<String>(); // 연산자용 리스트

	// 숫자 입력받는 리스너
	public void NumberActionListener(String number) 
	{
		// 만약에 numberOne이나 numberTwo의 마지막이 .인데 또 .이 들어올 경우에는 아무것도 안하고 그대로 지나간다.
		if(!numberOne.equals("")) 
		{
			//=을 누른 다음에 계산을 이어가는 부분
			if(RecordLastOut(recordLabel).equals("="))
			{
				numberOne="";
				numberTwo="";
				operation="";
				recordLabel="";
				answerRecord.setText("");
				this.number.clear();
				oper.clear();
			}
			if(number.equals(".")&&numberOne.substring(numberOne.length()-1,numberOne.length()).equals("."))
			{
				return;
			}
		}
		if(!numberTwo.equals(""))
		{
			if(number.equals(".")&&numberTwo.substring(numberTwo.length()-1,numberTwo.length()).equals("."))
			{
				return;
			}
		}
		//만약 맨 청므에 0이나오면 그 다음값으로 덮어씐다. 0 중복도 방지한다.
		if(numberOne.equals("0"))
		{
			numberOne="";
			recordLabel="";
		}
		else if(numberTwo.equals("0"))
		{
			numberTwo="";
			recordLabel=recordLabel.substring(0,recordLabel.length()-1);
		}
		//만약 맨 처음에 . 이 들어오면 자동으로 앞에 0을 붙여준다
		if(numberOne.equals(""))	
		{
			if(number.equals("."))
			{
				numberOne+="0";
				recordLabel+="0";
			}
		}
		//연산자가 있고, numberTwo에 들어갈 값이 처음에 . 일 경우
		else if(!operation.equals("")&&numberTwo.equals(""))
		{
			if(number.equals("."))
			{
				numberTwo+="0";
				recordLabel+="0";
			}
		}
		frame.requestFocus(); // 키이벤트를 받을 컴포넌트를 강제로 설정
		frame.setFocusable(true); // 컴포넌트가 여러개 있을 때 우선적으로 입력받기 위해 설정
		//if(numberOne.substring(0,1).equals("0")||numberTwo.substring(0,1).equals("."))
		if (operation == "") // 연산자 스트링에 값이 없으면 numberOne에 값을 추가한다.
		{
			this.numberOne += number;
			answerRecord.setText(numberOne);
			recordLabel += number; // 출력용 문자열에도 값을 추가해준다
		} 
		else // 연산자 스트링에 값이 있으면 numberTwo에 값을 추가한다
		{
			if (oper.size() == this.number.size()) // 숫자 리스트와 비교해서 연산자가 같을 때 연산자가 연속으로 리스트에 들어가지 않는다.
			{

			} else {
				oper.add(operation); // 연산자가 있기 때문에 리스트에 넣어서 확정한 뒤에 다른 업무를 수행한다.
			}
			recordLabel += number;
			this.numberTwo += number;
			answerRecord.setText(numberTwo); // numberTwo 값을 출력한다
		}
		
		reportRecord.setText(recordLabel); // 레코드 출력용 문자열을 출력
	}

	// 연산자를 눌렀을 떄 연산자를 연산자 문자열에 넣고, 앞의 숫자값들을 리스트에 넣어서 확정시킨다.
	public void CalActionListener(String cal) {
		//=누른다음에 연산자를 넣었을 때 계산을 이어서 한다
		if(RecordLastOut(recordLabel).equals("="))
		{
			numberOne=value;
			numberTwo="";
			operation="";
			number.clear();
			oper.clear();
			recordLabel=numberOne;
			answerRecord.setText(numberOne);
		}
		//.을 누른 다음 계산하면 .을 빼고 계산할 것
		if(numberOne.equals("")) 	//처음에 아무 값도 들어오지 않았을 때 0을 자동으로 넣는다
		{
		numberOne="0";	
		number.add(numberOne);
		recordLabel+="0";
		}
		if (operation.equals("")) // 연산자 문자열에 값이 없을 경우
		{
			if (this.oper.isEmpty()) // 연산자 리스트에 없을 경우, 초기 숫자값을 리스트에 넣어 확정시킨다
			{
				number.add(this.numberOne);
			} else {
				number.add(this.numberTwo); // 연산자가 있을 경우, 두번째 숫자값을 리스트에 넣어 확정시키고, 문자열을 초기화시킨다.
				numberTwo = "";
			}
			recordLabel += cal; // 연산자를 기록 출력 부분에 출력한다.
			this.operation = cal; // 연산자 문자열에 연산자를 넣는다
		} 
		//연산자값이 있는데 또 연산자를 입력할 때
		else if(RecordLastOut(recordLabel).equals("+")|| RecordLastOut(recordLabel).equals("-")
				|| RecordLastOut(recordLabel).equals("÷")|| RecordLastOut(recordLabel).equals("*"))
		{
				recordLabel = recordLabel.substring(0,recordLabel.length()- 1); // 뒤에 연산자 있는 부분을 자른다.
				recordLabel += cal; // 연산자를 기록 출력 부분에 출력한다.
				this.operation = cal; // 연산자 문자열에 연산자를 넣는다
		}
		else	// 연산자 문자열에 값이 있을 경우
		{
			
				number.add(this.numberTwo); // 연산자가 있을 경우, 두번째 숫자값을 리스트에 넣어 확정시키고, 문자열을 초기화시킨다.
				numberTwo = "";
				value = CalculatorMethod(
						number.get(number.size()- 2),
						number.get(number.size()- 1),
						oper.get(oper.size() - 1));
				number.clear();
				number.add(value);		// 계산한 값을 초기화해서 넣을 것
				answerRecord.setText(value);
				oper.clear();
			// recordLabel의 마지막 문자열을 빼서 그부분을 확인 비교하여 숫자인 경우에는 넘어간다. 연산자인 경우에만 덮어씌우기
			
			recordLabel += cal; // 연산자를 기록 출력 부분에 출력한다.
			this.operation = cal; // 연산자 문자열에 연산자를 넣는다
		}
		
		reportRecord.setText(recordLabel); // 레코드 출력용 문자열을 출력
	}

	// =버튼을 눌렀을 때
	public void EqualListner() {
		// 레코드의 마지막이 =일 때 마지막 계산을 반복한다.
		if (!recordLabel.isEmpty()&&RecordLastOut(recordLabel).equals("=")) 
		{
			//입력 없는 상태에서 계속 =을 누를 때 예외처리(0을 띄우고 그대로 둠)
			if(operation.equals(""))
			{
				answerRecord.setText("0");
				recordLabel="0=";
			}
			else	//입력 있고, 연산자 있는 상태에서 = 을 반복할 때
			{
				number.add(numberTwo);
				recordLabel = number.get(number.size()-2).toString()
					+ oper.get(oper.size() - 1).toString()
					+ number.get(number.size() - 1).toString()
					+ "=";
				value = CalculatorMethod(number.get(number.size()-2).toString(),
					number.get(number.size() - 1).toString(),
					oper.get(oper.size() - 1).toString());
				answerRecord.setText(value);
				reportRecord.setText(recordLabel);
				number.set(0, value);		//
				
			}
		}
		//만약 연산자까지만 입력된 상태에서 =을 누른다면
		else if(RecordLastOut(recordLabel).equals("+")||
				RecordLastOut(recordLabel).equals("-")||
				RecordLastOut(recordLabel).equals("*")||
				RecordLastOut(recordLabel).equals("÷"))
		{
			oper.add(operation);		//연산자값을 리스트에 넣어준다
			numberTwo=numberOne;
			number.add(numberTwo);
			value = CalculatorMethod(number.get(number.size()-2).toString(),
					number.get(number.size()-1).toString(),
					oper.get(oper.size()-1).toString());
			recordLabel=number.get(number.size()-2).toString()+
					oper.get(oper.size()-1).toString()+
					number.get(number.size()-1).toString()+"=";
			answerRecord.setText(value);
			reportRecord.setText(recordLabel);
		}
		//그냥 숫자가들어간 뒤에 = 누를 때
		else 
		{
			if (this.oper.isEmpty()) // 연산자 없을 때
			{
				if (this.numberOne == "") // 초기값이 없을 때 0을 넣고 그걸 계산식에 추가한다
				{
					int answer = 0;
					recordLabel = "0=";
					answerRecord.setText(String.valueOf(answer)); // 결과는 0이고 그 0값을 출력
					value=String.valueOf(answer);
					numberOne=value;
					number.add(numberOne);
				} else // 초기값이 있을 떄
				{
					recordLabel += "=";
					value=numberOne;// 결과는 초기값 그대로고 그 초기값을 그대로 출력
				}
				// 마지막에 =을 추가한다.
				reportRecord.setText(recordLabel); // 레코드 출력용 문자열을 출력
			} 
			else // 연산자 있을 때, list 0 값과 1 값을 계산한다
			{
				number.add(numberTwo);
				recordLabel += "=";
				value = "";
				value = CalculatorMethod(number.get(0).toString(),
						number.get(1).toString(),oper.get(0).toString());
				number.clear();
				number.add(value);
			}
			answerRecord.setText(value); // 최종 계산이 끝난 경우에는 그 결과를 정답존에 출력한다
			reportRecord.setText(recordLabel);
		}
	}
	

	// 초기화 리스너
	public void ClearListener() {
		this.number.clear();
		this.oper.clear();
		recordLabel = "";
		numberOne = "";
		numberTwo = "";
		operation = "";
		answerRecord.setText("");
		reportRecord.setText(recordLabel);
	}

	// 하나 지우기 기능 추가
	public void DeleteListener() {

		// 이부분 꼭 얘기할것!!!!!!!!!!!!!!!!!!!!!!!!!!!
		if (oper.isEmpty()
				&& !(numberOne.length() == 0)) {
			numberOne = numberOne.substring(0,
					numberOne.length() - 1);
			answerRecord.setText(numberOne);
		} else if (!(numberTwo.length() == 0)) {
			numberTwo = numberTwo.substring(0,
					numberTwo.length() - 1);
			answerRecord.setText(numberTwo);
		}
		if (!(recordLabel.length() == 0)) {
			String compete = RecordLastOut(
					recordLabel);
			if (!compete.equals("+")
					&& !compete.equals("-")
					&& !compete.equals("÷")
					&& !compete.equals("*")) {
				recordLabel = recordLabel
						.substring(0,
								recordLabel
										.length()
										- 1);
			}
		}
		reportRecord.setText(recordLabel);

	}

	// 숫자 2개와 연산자를 넣었을 떄 연산자값에 따라 자동으로 계산해주고 그 계산 결과를 반환하는 계산 메소드
	public String CalculatorMethod(String num1,
			String num2, String cal) {
		double numfront = Double.valueOf(num1); // 문자열로 들어온 숫자를 인트로 변환
		double numback = Double.valueOf(num2);
		String result = "";
		BigDecimal bigNumberfront = new BigDecimal(num1);
		BigDecimal bigNumberback = new BigDecimal(num2);
		switch (cal) // 각 사친연산을 수행한 뒤에 result에 값을 넣어 반환한다.
		{
		case "+":
			result = bigNumberfront.add(bigNumberback).toString();
			break;
		case "-":
			result = bigNumberfront.subtract(bigNumberback).toString();
			break;
		case "÷":
			if(bigNumberback.toString().equals("0")) 
			{
				return "0으로 나눌 수 없습니다.";
			}
			result = bigNumberfront.divide(bigNumberback,3,RoundingMode.HALF_EVEN).toString();	//BigDecimal을 써야 제대로 계산 된다
			break;
		case "*":
			result = bigNumberfront.multiply(bigNumberback).toString();
			break;
		}
		return result;
	}

	// record 스트링의 마지막 한글자 추출하기
	public String RecordLastOut(String record) {
		String result = record.substring(record.length() - 1,record.length());
		return result;
	}

}